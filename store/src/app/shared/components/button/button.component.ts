import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-button',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.scss'],
  styles: ['button {border: 2px solid green; background-color:rgba(153, 205, 50, 0.285); border-radius: 5px; color: green; padding: 5px 10px;}',
          'button:hover {color: rgba(153, 205, 50); background-color:green; cursor: pointer;}']
})
export class ButtonComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
