import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-input',
  templateUrl: './input.component.html',
  styleUrls: ['./input.component.scss'],
  styles: ['input {border: 2px solid green; border-radius: 5px; color: green; padding: 5px 10px; margin: 10px;}',
          'input:hover {color: green; background-color: rgba(0, 0, 0, 0.025); cursor: pointer;}']
})
export class InputComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
