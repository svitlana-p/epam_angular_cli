import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProductComponent } from './product/product.component';
import { ButtonModule } from 'src/app/shared/components/button/button.module';
import { InputModule } from 'src/app/shared/components/input/input.module';
import { ProductsComponent } from './products.component';

@NgModule({
  declarations: [
    ProductComponent,
    ProductsComponent
  ],
  imports: [
    CommonModule,
    ButtonModule,
    InputModule   
  ],
  exports: [
    ProductsComponent,
    ProductComponent,
   
  ]
})
export class ProductsModule { }
